import { Home } from "pages/Home";
import { Info } from "pages/Info";

const navigationRoutes = [
  { path: "/", title: "Home", component: Home },
  { path: "/info", title: "Info", component: Info },
];

export { navigationRoutes };

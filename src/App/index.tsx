import React, { useContext, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";

import { MainContainer } from "./styled";
import { Header } from "components/Header";
import { Footer } from "components/Footer";
import { Pane } from "components/Pane";

import { NavigationSwitch } from "App/NavigationSwitch";
import { OperationsStoreContext } from "stores/OperationsStore";
import { observer } from "mobx-react-lite";

const App = observer(() => {
  const OperationsStore = useContext(OperationsStoreContext);

  useEffect(() => {
    OperationsStore.fetch();
  }, [OperationsStore.isAuth]);

  return (
    <MainContainer>
      <Router>
        <Pane>
          <Header />
          <NavigationSwitch />
        </Pane>
        <Footer />
      </Router>
    </MainContainer>
  );
});

export { App };
